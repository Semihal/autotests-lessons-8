package grid.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class StaffPage extends HomePage
{
    private static final String PAGE_URL = "portal.jet.su/company";

    @FindBy(name = "company_search_FIO")
    private WebElement FIOInput;

    @FindBy(xpath = "//div//input[@name='set_filter_company_search' and @value='Найти']")
    private WebElement searchButton;


    public StaffPage(WebDriver webDriver) throws Exception {
        super(webDriver);

        if (!webDriver.getCurrentUrl().contains(PAGE_URL)) {
            throw new Exception("This is not staff page.");
        }
    }

    public PersonalPage searchEmployee(String FIO, int userId) throws Exception {
        FIOInput.sendKeys(FIO);
        searchButton.click();

        WebElement userCard = (new WebDriverWait(driver, 5))
                .until(ExpectedConditions.presenceOfElementLocated(By.id("system_person_" + userId)));
        userCard.click();

        return new PersonalPage(driver);
    }
}

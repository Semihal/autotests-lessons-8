package grid.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage
{
    protected WebDriver driver;

    @FindBy(xpath = "//div//a[@href='/company/']")
    private WebElement staffNavigateButton;

    @FindBy(id = "user-logout")
    private WebElement logoutButton;

    public HomePage(WebDriver webDriver) throws Exception {
        this.driver = webDriver;
        initFactory();
    }

    private void initFactory()
    {
        PageFactory.initElements(driver, this);
    }

    public StaffPage getStaffPage() throws Exception {
        staffNavigateButton.click();
        return new StaffPage(driver);
    }

    public LoginPage logout() throws Exception {
        logoutButton.click();
        return new LoginPage(driver);
    }
}

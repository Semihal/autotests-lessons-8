package grid.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage
{
    private WebDriver driver;

    @FindBy(name = "USER_LOGIN")
    private WebElement userInput;

    @FindBy(name = "USER_PASSWORD")
    private WebElement passwordInput;

    @FindBy(className = "login-btn")
    private WebElement loginSubmit;

    public LoginPage(WebDriver webDriver) throws Exception {
        this.driver = webDriver;
        PageFactory.initElements(driver, this);
    }

    public LoginPage inputUsername(String username)
    {
        userInput.sendKeys(username);
        return this;
    }

    public LoginPage inputPassword(String password)
    {
        passwordInput.sendKeys(password);
        return this;
    }

    public HomePage submitLogin() throws Exception {
        loginSubmit.click();
        return new HomePage(this.driver);
    }
}

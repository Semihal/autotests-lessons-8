package grid.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PersonalPage extends HomePage
{
    private static final String PAGE_URL = "portal.jet.su/company/personal/user";

    @FindBy(partialLinkText = "@jet.msk.su")
    private WebElement emailField;

    public PersonalPage(WebDriver webDriver) throws Exception {
        super(webDriver);
    }

    public String getEmail()
    {
        return this.emailField.getText();
    }
}

package grid;

import com.google.code.tempusfugit.concurrency.ConcurrentTestRunner;
import grid.page.HomePage;
import grid.page.LoginPage;
import grid.page.PersonalPage;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

@RunWith(ConcurrentTestRunner.class)
public class PortalTest {

    private static final String HUB_URL = "http://semihal.servehttp.com:4444/wd/hub";

    @Test
    public void chrome_searchEmployeeByNameAndNumberAndCheckEmail_EmailIsEquals() throws Exception {
        WebDriver driver = getWebDriver(BrowserType.CHROME);

        String email = getEmployeeEmail(driver);
        Assert.assertEquals("Email is invalid","si.mikhaylin@jet.msk.su", email);

        driver.quit();
    }

    @Test
    public void firefox_searchEmployeeByNameAndNumberAndCheckEmail_EmailIsEquals() throws Exception {
        WebDriver driver = getWebDriver(BrowserType.FIREFOX);

        String email = getEmployeeEmail(driver);
        Assert.assertEquals("Email is invalid","si.mikhaylin@jet.msk.su", email);

        driver.quit();
    }

    private String getEmployeeEmail(WebDriver driver) throws Exception {
        driver.get("https://portal.jet.su");

        LoginPage loginPage = new LoginPage(driver);
        loginPage.inputUsername(System.getenv("username"));
        loginPage.inputPassword(System.getenv("password"));
        HomePage portal = loginPage.submitLogin();

        int userId = Integer.parseInt(System.getenv("userId")); // 7496
        PersonalPage personalPage = portal.getStaffPage().searchEmployee("Михайлин Сергей", userId);
        String email = personalPage.getEmail();

        portal.logout();
        return email;
    }

    private WebDriver getWebDriver(String browserType) throws MalformedURLException
    {
        DesiredCapabilities capabilities;
        if (browserType.equals(BrowserType.FIREFOX)) {
            capabilities = getFirefoxCapabilities();
        } else {
            capabilities = getChromeCapabilities();
        }

        URL hostURL = new URL(HUB_URL);
        WebDriver driver = new RemoteWebDriver(hostURL, capabilities);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        return driver;
    }

    private DesiredCapabilities getChromeCapabilities()
    {
        DesiredCapabilities capability = DesiredCapabilities.chrome();
        capability.setBrowserName("chrome");
        capability.setPlatform(Platform.LINUX);
        capability.setVersion("63.0.3239.84");

        return capability;
    }

    private DesiredCapabilities getFirefoxCapabilities()
    {
        DesiredCapabilities capability = DesiredCapabilities.firefox();
        capability.setBrowserName("firefox");
        capability.setPlatform(Platform.LINUX);
        capability.setVersion("57.0.2");

        return capability;
    }
}
